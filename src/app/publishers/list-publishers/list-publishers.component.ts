import { Component } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { DatabaseService } from "src/app/database/database.service";
import { Publisher } from "src/app/database/entities/publisher";

@Component({
  selector: "app-list-publishers",
  templateUrl: "./list-publishers.component.html",
  styleUrls: ["./list-publishers.component.scss"],
})
export class ListPublishersComponent {
  publishers: Publisher[];
  dataSource: MatTableDataSource<Publisher>;

  constructor(private databaseService: DatabaseService) {
    this.databaseService.connection
      .getRepository(Publisher)
      .find({ order: { lastName: "ASC", firstName: "ASC" } })
      .then((publishers) => {
        this.publishers = publishers;
        this.dataSource = new MatTableDataSource(this.publishers);
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
