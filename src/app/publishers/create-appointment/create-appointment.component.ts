import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { DatabaseService } from "src/app/database/database.service";
import { Appointment, AppointmentType } from "src/app/database/entities/appointment";
import { Publisher } from "src/app/database/entities/publisher";

@Component({
  selector: "app-create-appointment",
  templateUrl: "./create-appointment.component.html",
  styleUrls: ["./create-appointment.component.scss"],
})
export class CreateAppointmentComponent implements OnInit {
  publisher: Publisher;
  form: FormGroup;
  appointmentTypes = AppointmentType;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private databaseService: DatabaseService,
    private snackBar: MatSnackBar
  ) {
    this.form = this.formBuilder.group({
      type: [null, Validators.required],
      startDate: [null, Validators.required],
      endDate: [null],
    });
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get("id")!;

    this.databaseService.connection
      .getRepository(Publisher)
      .findOne(id)
      .then((publisher) => {
        this.publisher = publisher!;
      });
  }

  get type() {
    return this.form.get("type");
  }

  get startDate() {
    return this.form.get("startDate");
  }

  get endDate() {
    return this.form.get("endDate");
  }

  async handleSubmit() {
    if (!this.form.valid) {
      return;
    }

    await this.databaseService.connection
      .getRepository(Appointment)
      .insert({ ...this.form.value, publisher: this.publisher });
    this.snackBar.open("Nomination ajoutée", "Ok", { duration: 5000 });
    this.router.navigate(["/publishers", "view-publisher", this.publisher.id]);
  }
}
