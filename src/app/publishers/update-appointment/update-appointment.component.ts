import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { DatabaseService } from "src/app/database/database.service";
import { Appointment, AppointmentType } from "src/app/database/entities/appointment";
import { ConfirmDialogComponent } from "src/app/shared/confirm-dialog/confirm-dialog.component";

@Component({
  selector: "app-update-appointment",
  templateUrl: "./update-appointment.component.html",
  styleUrls: ["./update-appointment.component.scss"],
})
export class UpdateAppointmentComponent implements OnInit {
  public form: FormGroup;
  appointment: Appointment;
  appointmentTypes = AppointmentType;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private databaseService: DatabaseService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.form = this.formBuilder.group({
      type: [null, Validators.required],
      startDate: [null, Validators.required],
      endDate: [null],
    });
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get("id")!;

    this.databaseService.connection
      .getRepository(Appointment)
      .findOne(id, { relations: ["publisher"] })
      .then((appointment) => {
        this.appointment = appointment!;
        this.form.patchValue(this.appointment);
      });
  }

  get type() {
    return this.form.get("type");
  }

  get startDate() {
    return this.form.get("startDate");
  }

  get endDate() {
    return this.form.get("endDate");
  }

  async handleSubmit() {
    if (!this.form.valid) {
      return;
    }

    await this.databaseService.connection
      .getRepository(Appointment)
      .update(this.appointment.id, this.form.value);

    this.snackBar.open("Modifications enregistrées", "Ok", { duration: 5000 });
    this.router.navigate([
      "/publishers",
      "view-publisher",
      this.appointment.publisher.id,
    ]);
  }

  deleteAppointment() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: "Confirmez la suppressions de la nomination",
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result && this.appointment) {
        this.databaseService.connection
          .getRepository(Appointment)
          .delete(this.appointment.id)
          .then(() => {
            this.snackBar.open("Nomination supprimée", "Ok", {
              duration: 5000,
            });
            this.router.navigate([
              "/publishers",
              "view-publisher",
              this.appointment.publisher.id,
            ]);
          });
      }
    });
  }
}
