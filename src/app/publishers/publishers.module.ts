import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PublishersRoutingModule } from "./publishers-routing.module";
import { ListPublishersComponent } from "./list-publishers/list-publishers.component";
import { SharedModule } from "../shared/shared.module";
import { CreatePublisherComponent } from './create-publisher/create-publisher.component';
import { ViewPublisherComponent } from './view-publisher/view-publisher.component';
import { UpdatePublisherComponent } from './update-publisher/update-publisher.component';
import { ImportPublishersComponent } from './import-publishers/import-publishers.component';
import { CreateAppointmentComponent } from './create-appointment/create-appointment.component';
import { UpdateAppointmentComponent } from './update-appointment/update-appointment.component';
import { CreateActivityComponent } from './create-activity/create-activity.component';
import { UpdateActivityComponent } from './update-activity/update-activity.component';

@NgModule({
  declarations: [ListPublishersComponent, CreatePublisherComponent, ViewPublisherComponent, UpdatePublisherComponent, ImportPublishersComponent, CreateAppointmentComponent, UpdateAppointmentComponent, CreateActivityComponent, UpdateActivityComponent],
  imports: [CommonModule, SharedModule, PublishersRoutingModule],
})
export class PublishersModule {}
