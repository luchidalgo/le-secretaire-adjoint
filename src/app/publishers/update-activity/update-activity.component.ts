import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { DatabaseService } from 'src/app/database/database.service';
import { Activity } from 'src/app/database/entities/activity';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-update-activity',
  templateUrl: './update-activity.component.html',
  styleUrls: ['./update-activity.component.scss']
})
export class UpdateActivityComponent implements OnInit {
  public form: FormGroup;
  activity: Activity;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private databaseService: DatabaseService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.form = this.formBuilder.group({
      placements: [null, Validators.required],
      videos: [null, Validators.required],
      hours: [null, Validators.required],
      visits: [null, Validators.required],
      studies: [null, Validators.required],
      remarks: [null],
    });
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get("id")!;

    this.databaseService.connection
      .getRepository(Activity)
      .findOne(id, { relations: ["publisher"] })
      .then((activity) => {
        this.activity = activity!;
        this.form.patchValue(this.activity);
      });
  }

  get placements() {
    return this.form.get("placements");
  }

  get videos() {
    return this.form.get("videos");
  }

  get hours() {
    return this.form.get("hours");
  }

  get visits() {
    return this.form.get("visits");
  }

  get studies() {
    return this.form.get("studies");
  }

  get remarks() {
    return this.form.get("remarks");
  }

  async handleSubmit() {
    if (!this.form.valid) {
      return;
    }

    await this.databaseService.connection
      .getRepository(Activity)
      .update(this.activity.id, this.form.value);

    this.snackBar.open("Modifications enregistrées", "Ok", { duration: 5000 });
    this.router.navigate([
      "/publishers",
      "view-publisher",
      this.activity.publisher.id,
    ]);
  }

  deleteActivity() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: "Confirmez la suppressions de cette activité",
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result && this.activity) {
        this.databaseService.connection
          .getRepository(Activity)
          .delete(this.activity.id)
          .then(() => {
            this.snackBar.open("Activité supprimée", "Ok", {
              duration: 5000,
            });
            this.router.navigate([
              "/publishers",
              "view-publisher",
              this.activity.publisher.id,
            ]);
          });
      }
    });
  }
}
