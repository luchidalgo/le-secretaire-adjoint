import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CreateActivityComponent } from "./create-activity/create-activity.component";
import { CreateAppointmentComponent } from "./create-appointment/create-appointment.component";
import { CreatePublisherComponent } from "./create-publisher/create-publisher.component";
import { ImportPublishersComponent } from "./import-publishers/import-publishers.component";
import { ListPublishersComponent } from "./list-publishers/list-publishers.component";
import { UpdateActivityComponent } from "./update-activity/update-activity.component";
import { UpdateAppointmentComponent } from "./update-appointment/update-appointment.component";
import { UpdatePublisherComponent } from "./update-publisher/update-publisher.component";
import { ViewPublisherComponent } from "./view-publisher/view-publisher.component";

const routes: Routes = [
  { path: "", component: ListPublishersComponent },
  { path: "create-publisher", component: CreatePublisherComponent },
  { path: "import-publishers", component: ImportPublishersComponent },
  { path: "view-publisher/:id", component: ViewPublisherComponent },
  { path: "update-publisher/:id", component: UpdatePublisherComponent },
  { path: "create-appointment/:id", component: CreateAppointmentComponent },
  { path: "update-appointment/:id", component: UpdateAppointmentComponent },
  { path: "create-activity/:id", component: CreateActivityComponent },
  { path: "update-activity/:id", component: UpdateActivityComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublishersRoutingModule {}
