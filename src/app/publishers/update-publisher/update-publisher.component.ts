import { Component, OnInit } from "@angular/core";
import {
  AsyncValidatorFn,
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators,
} from "@angular/forms";
import { ShowOnDirtyErrorStateMatcher } from "@angular/material/core";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { DatabaseService } from "src/app/database/database.service";
import { Publisher } from "src/app/database/entities/publisher";
import { ConfirmDialogComponent } from "src/app/shared/confirm-dialog/confirm-dialog.component";

@Component({
  selector: "app-update-publisher",
  templateUrl: "./update-publisher.component.html",
  styleUrls: ["./update-publisher.component.scss"],
})
export class UpdatePublisherComponent implements OnInit {
  public form: FormGroup;
  public publisherAlreadyExistsErrorStateMatcher =
    new PublisherAlreadyExistsErrorStateMatcher();
  publisher: Publisher;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private databaseService: DatabaseService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.form = this.formBuilder.group(
      {
        firstName: [null, Validators.required],
        lastName: [null, Validators.required],
        gender: [null, Validators.required],
        baptismDate: [null],
        birthDate: [null],
        isAnointed: [null, Validators.required],
        isInactive: [null, Validators.required],
      },
      {
        asyncValidators: this.publisherAlreadyExistsValidatorFn(),
      }
    );
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get("id")!;

    this.databaseService.connection
      .getRepository(Publisher)
      .findOne(id)
      .then((publisher) => {
        this.publisher = publisher!;
        this.form.patchValue(this.publisher);
      });
  }

  get firstName() {
    return this.form.get("firstName");
  }
  get lastName() {
    return this.form.get("lastName");
  }
  get baptismDate() {
    return this.form.get("baptismDate");
  }
  get birthDate() {
    return this.form.get("birthDate");
  }
  get gender() {
    return this.form.get("gender");
  }
  get isAnointed() {
    return this.form.get("isAnointed");
  }
  get isInactive() {
    return this.form.get("isInactive");
  }

  async handleSubmit() {
    if (!this.form.valid) {
      return;
    }

    await this.databaseService.connection
      .getRepository(Publisher)
      .update(this.publisher.id, this.form.value);

    this.snackBar.open("Modifications enregistrées", "Ok", { duration: 5000 });
    this.router.navigate(["/publishers", "view-publisher", this.publisher.id]);
  }

  publisherAlreadyExistsValidatorFn(): AsyncValidatorFn {
    return (control) => {
      const firstName = control.value.firstName;
      const lastName = control.value.lastName;
      const id = this.publisher?.id;

      if (!firstName || !lastName || !id) {
        return Promise.resolve(null);
      }

      return this.databaseService.connection
        .getRepository(Publisher)
        .createQueryBuilder("publisher")
        .where("publisher.id != :id", { id })
        .andWhere("publisher.firstName LIKE :firstName", { firstName })
        .andWhere("publisher.lastName LIKE :lastName", { lastName })
        .getOne()
        .then((publisher) => {
          if (publisher) {
            return { publisherAlreadyExists: true };
          }

          return null;
        });
    };
  }

  deletePublisher() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: "Confirmez la suppressions du proclamateur",
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result && this.publisher) {
        this.databaseService.connection
          .getRepository(Publisher)
          .delete(this.publisher.id)
          .then(() => {
            this.snackBar.open("Proclamateur supprimé", "Ok", {
              duration: 5000,
            });
            this.router.navigate(["/publishers"]);
          });
      }
    });
  }
}

class PublisherAlreadyExistsErrorStateMatcher extends ShowOnDirtyErrorStateMatcher {
  override isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    return (
      super.isErrorState(control, form) ||
      (control &&
        control.parent &&
        control.parent.getError("publisherAlreadyExists"))
    );
  }
}
