import { Component, OnInit } from "@angular/core";
import {
  AsyncValidatorFn,
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";
import { MAT_DATE_FORMATS } from "@angular/material/core";
import { MatDatepicker } from "@angular/material/datepicker";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { DatabaseService } from "src/app/database/database.service";
import { Activity } from "src/app/database/entities/activity";
import { Publisher } from "src/app/database/entities/publisher";

const MONTH_FORMATS = {
  parse: {
    dateInput: "MM/yyyy",
  },
  display: {
    dateInput: "MM/yyyy",
    monthYearLabel: "MMM yyyy",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM yyyy",
  },
};

@Component({
  selector: "app-create-activity",
  templateUrl: "./create-activity.component.html",
  styleUrls: ["./create-activity.component.scss"],
  providers: [{ provide: MAT_DATE_FORMATS, useValue: MONTH_FORMATS }],
})
export class CreateActivityComponent implements OnInit {
  publisher: Publisher;
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private databaseService: DatabaseService,
    private snackBar: MatSnackBar
  ) {
    this.form = this.formBuilder.group({
      month: [
        null,
        Validators.required,
        [this.activityAlreadyExistsValidatorFn()],
      ],
      placements: [null, Validators.required],
      videos: [null, Validators.required],
      hours: [null, Validators.required],
      visits: [null, Validators.required],
      studies: [null, Validators.required],
      remarks: [null],
    });
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get("id")!;

    this.databaseService.connection
      .getRepository(Publisher)
      .findOne(id)
      .then((publisher) => {
        this.publisher = publisher!;
      });
  }

  get month() {
    return this.form.get("month");
  }

  get placements() {
    return this.form.get("placements");
  }

  get videos() {
    return this.form.get("videos");
  }

  get hours() {
    return this.form.get("hours");
  }

  get visits() {
    return this.form.get("visits");
  }

  get studies() {
    return this.form.get("studies");
  }

  get remarks() {
    return this.form.get("remarks");
  }

  async handleSubmit() {
    if (!this.form.valid) {
      return;
    }

    await this.databaseService.connection
      .getRepository(Activity)
      .insert({ ...this.form.value, publisher: this.publisher });
    this.snackBar.open("Activité ajoutée", "Ok", { duration: 5000 });
    this.router.navigate(["/publishers", "view-publisher", this.publisher.id]);
  }

  chosenMonthHandler(date: Date, datepicker: MatDatepicker<Date>) {
    this.month?.setValue(date);
    datepicker.close();
  }

  activityAlreadyExistsValidatorFn(): AsyncValidatorFn {
    return (control) => {
      const month: Date = control.value;

      if (!month || !this.publisher) {
        return Promise.resolve(null);
      }

      return this.databaseService.connection
        .getRepository(Activity)
        .createQueryBuilder("activity")
        .where("activity.publisherId = :publisherId", {
          publisherId: this.publisher.id,
        })
        .andWhere("DATE(activity.month) = DATE(:month)", { month: month.toISOString() })
        .getOne()
        .then((activity) => {
          if (activity) {
            return { activityAlreadyExists: true };
          }

          return null;
        });
    };
  }
}
