import { Component } from "@angular/core";
import {
  AsyncValidatorFn,
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators,
} from "@angular/forms";
import { ShowOnDirtyErrorStateMatcher } from "@angular/material/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { Console } from "console";
import { DatabaseService } from "src/app/database/database.service";
import { Publisher } from "src/app/database/entities/publisher";

@Component({
  selector: "app-create-publisher",
  templateUrl: "./create-publisher.component.html",
  styleUrls: ["./create-publisher.component.scss"],
})
export class CreatePublisherComponent {
  public form: FormGroup;
  public publisherAlreadyExistsErrorStateMatcher =
    new PublisherAlreadyExistsErrorStateMatcher();

  constructor(
    private formBuilder: FormBuilder,
    private databaseService: DatabaseService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.form = this.formBuilder.group(
      {
        firstName: [null, Validators.required],
        lastName: [null, Validators.required],
        gender: [null, Validators.required],
        baptismDate: [null],
        birthDate: [null],
        isAnointed: [null, Validators.required],
        isInactive: [null, Validators.required],
      },
      {
        asyncValidators: this.publisherAlreadyExistsValidatorFn(),
      }
    );
  }

  get firstName() {
    return this.form.get("firstName");
  }
  get lastName() {
    return this.form.get("lastName");
  }
  get baptismDate() {
    return this.form.get("baptismDate");
  }
  get birthDate() {
    return this.form.get("birthDate");
  }
  get gender() {
    return this.form.get("gender");
  }
  get isAnointed() {
    return this.form.get("isAnointed");
  }
  get isInactive() {
    return this.form.get("isInactive");
  }

  async handleSubmit() {
    if (!this.form.valid) {
      return;
    }

    const publisherId = await this.databaseService.connection
      .getRepository(Publisher)
      .insert(this.form.value)
      .then((insertResult) => insertResult.raw);

    this.snackBar.open("Proclamateur ajouté", "Ok", { duration: 5000 });
    this.router.navigate(["/publishers", "view-publisher", publisherId]);
  }

  publisherAlreadyExistsValidatorFn(): AsyncValidatorFn {
    return (control) => {
      const firstName = control.value.firstName;
      const lastName = control.value.lastName;

      if (!firstName || !lastName) {
        return Promise.resolve(null);
      }

      return this.databaseService.connection
        .getRepository(Publisher)
        .createQueryBuilder("publisher")
        .andWhere("publisher.firstName LIKE :firstName", { firstName })
        .andWhere("publisher.lastName LIKE :lastName", { lastName })
        .getOne()
        .then((publisher) => {
          if (publisher) {
            return { publisherAlreadyExists: true };
          }

          return null;
        });
    };
  }
}

class PublisherAlreadyExistsErrorStateMatcher extends ShowOnDirtyErrorStateMatcher {
  override isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    return (
      super.isErrorState(control, form) ||
      (control &&
        control.parent &&
        control.parent.getError("publisherAlreadyExists"))
    );
  }
}
