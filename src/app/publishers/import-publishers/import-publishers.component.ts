import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import readXlsxFile from "read-excel-file";
import { DatabaseService } from "src/app/database/database.service";
import { Publisher } from "src/app/database/entities/publisher";
import { ErrorsDialogComponent } from "src/app/shared/errors-dialog/errors-dialog.component";

@Component({
  selector: "app-import-publishers",
  templateUrl: "./import-publishers.component.html",
  styleUrls: ["./import-publishers.component.scss"],
})
export class ImportPublishersComponent {
  public form: FormGroup;
  @ViewChild("fileInput") fileInput: ElementRef<HTMLInputElement>;
  dataSource = importPublishersFileFormat;

  constructor(
    private formBuilder: FormBuilder,
    private databaseService: DatabaseService,
    private dialog: MatDialog,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.form = this.formBuilder.group({
      file: [null, Validators.required],
    });
  }

  get file() {
    return this.form.get("file");
  }

  onFileInputCHange() {
    let file = null;

    if (
      this.fileInput.nativeElement.files &&
      this.fileInput.nativeElement.files.length
    ) {
      file = this.fileInput.nativeElement.files[0].name;
    }

    this.form.patchValue({ file });
  }

  async handleSubmit() {
    if (!this.form.valid) {
      return;
    }

    const file = this.fileInput.nativeElement.files?.item(0);
    if (!file) {
      return;
    }

    const { rows, errors } = await readXlsxFile<ImportPublishersFileRow>(file, {
      schema: importPublishersFileSchema,
    });

    if (errors.length > 0) {
      this.showErrors(
        errors.map(
          (error) => `
          Ligne ${error.row}, colonne ${error.column} : 
          donnée ${error.error === "required" ? "manquante" : "incorrecte"}
          `
        )
      );

      this.resetInputFile();
      return;
    }

    const databaseErrors: string[] = [];
    for (const row of rows) {
      const publisher = await this.databaseService.connection
        .getRepository(Publisher)
        .createQueryBuilder("publisher")
        .andWhere("publisher.firstName LIKE :firstName", { firstName: row.firstName })
        .andWhere("publisher.lastName LIKE :lastName", { lastName: row.lastName })
        .getOne();

      if (publisher) {
        databaseErrors.push(
          `Le proclamateur ${row.lastName} ${row.firstName} existe déjà dans la base de donnée`
        );
      }
    }

    if (databaseErrors.length > 0) {
      this.showErrors(databaseErrors);
      this.resetInputFile();
      return;
    }

    for (const row of rows) {
      await this.databaseService.connection
      .getRepository(Publisher)
      .insert(this.convertRowToPublisher(row))
    }

    this.snackBar.open("Proclamateurs importés", "Ok", { duration: 5000 });
    this.router.navigate(["/publishers"]);
  }

  convertRowToPublisher(row: ImportPublishersFileRow): Partial<Publisher> {
    return {
      ...row,
      gender: row.gender === 'Homme' ? 'male' : 'female',
      isAnointed: row.isAnointed === 'Oint',
      isInactive: false
    };
  }

  resetInputFile() {
    this.fileInput.nativeElement.value = "";
    this.fileInput.nativeElement.dispatchEvent(new Event('change'));
  }

  showErrors(errors: string[]) {
    const data = {
      title: "Erreur d'importation",
      message:
        "L'importation n'a pas fonctionnée. Corrigez les erreurs suivantes puis réessayez.",
      errors: errors,
    };

    this.dialog.open(ErrorsDialogComponent, { data });
  }
}

interface ImportPublishersFileRow {
  firstName: string;
  lastName: string;
  gender: "Homme" | "Femme";
  isAnointed: "Oint" | "Autre brebis";
  birthDate?: Date;
  baptismDate?: Date;
}

const importPublishersFileSchema = {
  Nom: {
    prop: "lastName",
    type: String,
    required: true,
  },
  Prénom: {
    prop: "firstName",
    type: String,
    required: true,
  },
  Genre: {
    prop: "gender",
    type: String,
    required: true,
    oneOf: ["Homme", "Femme"],
  },
  Espérance: {
    prop: "isAnointed",
    type: String,
    required: true,
    oneOf: ["Oint", "Autre brebis"],
  },
  "Date de naissance": {
    prop: "birthDate",
    type: Date,
    required: false,
  },
  "Date de baptème": {
    prop: "baptismDate",
    type: Date,
    required: false,
  },
};

const importPublishersFileFormat: {
  position: number;
  field: string;
  format: string;
  required: boolean;
}[] = [
  { position: 1, field: 'Nom', format: 'Texte', required: true },
  { position: 2, field: 'Prénom', format: 'Texte', required: true },
  { position: 3, field: 'Genre', format: 'Homme / Femme', required: true },
  { position: 4, field: 'Espérance', format: 'Oint / Autre brebis', required: true },
  { position: 5, field: 'Date de naissance', format: 'Date', required: false },
  { position: 6, field: 'Date de baptème', format: 'Date', required: false },
];
