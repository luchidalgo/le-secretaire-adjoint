import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { DatabaseService } from "src/app/database/database.service";
import { Activity } from "src/app/database/entities/activity";
import {
  Appointment,
  AppointmentType,
} from "src/app/database/entities/appointment";
import { Publisher } from "src/app/database/entities/publisher";

@Component({
  selector: "app-view-publisher",
  templateUrl: "./view-publisher.component.html",
  styleUrls: ["./view-publisher.component.scss"],
})
export class ViewPublisherComponent implements OnInit {
  publisher: Publisher;
  appointments: Appointment[];
  activities: Activity[];
  appointmentTypes = AppointmentType;

  constructor(
    private route: ActivatedRoute,
    private databaseService: DatabaseService
  ) {}

  async ngOnInit() {
    const id = this.route.snapshot.paramMap.get("id")!;

    this.publisher = await this.databaseService.connection
      .getRepository(Publisher)
      .findOneOrFail(id);

    this.appointments = await this.databaseService.connection
      .getRepository(Appointment)
      .createQueryBuilder("appointment")
      .where("appointment.publisherId = :publisherId", {
        publisherId: this.publisher.id,
      })
      .orderBy({
        "appointment.endDate": { order: "DESC", nulls: "NULLS FIRST" },
        "appointment.startDate": "DESC",
      })
      .getMany();

    this.activities = await this.databaseService.connection
      .getRepository(Activity)
      .createQueryBuilder("activity")
      .where("activity.publisherId = :publisherId", {
        publisherId: this.publisher.id,
      })
      .orderBy({
        "activity.month": "DESC",
      })
      .getMany();
  }
}
