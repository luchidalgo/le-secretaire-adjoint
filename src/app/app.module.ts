import { APP_INITIALIZER, LOCALE_ID, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { SharedModule } from "./shared/shared.module";
import { registerLocaleData } from "@angular/common";
import initializeDatabase from "./database/database.initializer";
import { DatabaseService } from "./database/database.service";
import {
  MatFormFieldDefaultOptions,
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
} from "@angular/material/form-field";
import localeFr from "@angular/common/locales/fr";
import { DatabaseModule } from "./database/database.module";
import { MAT_DATE_LOCALE } from "@angular/material/core";
import { fr } from 'date-fns/locale';

registerLocaleData(localeFr, "fr");

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    DatabaseModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: "fr",
    },
    {
      provide: MAT_DATE_LOCALE,
      useValue: fr,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeDatabase,
      deps: [DatabaseService],
      multi: true,
    },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: "outline" } as MatFormFieldDefaultOptions,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
