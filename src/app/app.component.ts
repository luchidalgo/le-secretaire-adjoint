import { Component } from '@angular/core';
import { map, Observable } from 'rxjs';
import { DatabaseService } from './database/database.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  openMenu$: Observable<boolean>;

  constructor(
    private databaseService: DatabaseService
  ) {
    this.openMenu$ = this.databaseService.openedDatabase$.pipe(
      map(openedDatabase => !!openedDatabase)
    );
  }
}
