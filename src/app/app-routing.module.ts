import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DatabaseGuard } from "./database/database.guard";
import { SelectDatabaseComponent } from "./database/select-database/select-database.component";

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/activities",
  },
  {
    path: "select-database",
    component: SelectDatabaseComponent,
  },
  {
    path: "publishers",
    canActivateChild: [DatabaseGuard],
    loadChildren: () =>
      import("./publishers/publishers.module").then((m) => m.PublishersModule),
  },
  {
    path: "activities",
    canActivateChild: [DatabaseGuard],
    loadChildren: () =>
      import("./activities/activities.module").then((m) => m.ActivitiesModule),
  },
  { path: "**", redirectTo: "" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
