import {
  DatabaseService,
  LAST_DATABASE_PATH_LOCAL_STORAGE_KEY,
} from "./database.service";
import * as fs from "fs";

export default function initializeDatabase(databaseService: DatabaseService) {
  return () => {
    const lastDatabasePath = localStorage.getItem(
      LAST_DATABASE_PATH_LOCAL_STORAGE_KEY
    );

    if (!lastDatabasePath) {
      return;
    }

    if (!fs.existsSync(lastDatabasePath)) {
      localStorage.removeItem(LAST_DATABASE_PATH_LOCAL_STORAGE_KEY);
      return;
    }

    return databaseService.openDatabase(lastDatabasePath).catch((error) => {
      localStorage.removeItem(LAST_DATABASE_PATH_LOCAL_STORAGE_KEY);
      console.error(error);
    });
  };
}
