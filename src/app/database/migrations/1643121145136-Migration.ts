import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1643121145136 implements MigrationInterface {
  async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      CREATE TABLE "publisher" (
        "id" INTEGER NOT NULL PRIMARY KEY,
        "firstName" VARCHAR NOT NULL,
        "lastName" VARCHAR NOT NULL,
        "birthDate" DATETIME NULL,
        "baptismDate" DATETIME NULL,
        "gender" VARCHAR NOT NULL,
        "isAnointed" TINYINT NOT NULL,
        "isInactive" TINYINT NOT NULL
      );
    `);

    await queryRunner.query(`
      CREATE UNIQUE INDEX "unique_publisher" 
      ON "publisher" ("firstName", "lastName");
    `);

    await queryRunner.query(`
      CREATE TABLE "activity" (
        "id" INTEGER NOT NULL PRIMARY KEY,
        "month" DATETIME NOT NULL,
        "placements" INTEGER NOT NULL,
        "videos" INTEGER NOT NULL,
        "hours" INTEGER NOT NULL,
        "visits" INTEGER NOT NULL,
        "studies" INTEGER NOT NULL,
        "remarks" VARCHAR NULL,
        "publisherId" INTEGER NULL,
        FOREIGN KEY ("publisherId") REFERENCES "publisher" ("id") ON DELETE CASCADE
      );
    `);

    await queryRunner.query(`
      CREATE UNIQUE INDEX "unique_activity" 
      ON "activity" ("publisherId", "month");
    `);

    await queryRunner.query(`
      CREATE TABLE "appointment" (
        "id" INTEGER NOT NULL PRIMARY KEY,
        "type" VARCHAR NOT NULL,
        "startDate" DATETIME NOT NULL,
        "endDate" DATETIME NULL,
        "publisherId" INTEGER NULL,
        FOREIGN KEY ("publisherId") REFERENCES "publisher" ("id") ON DELETE CASCADE
      );
    `);
  }

  async down(queryRunner: QueryRunner): Promise<void> {
    throw new Error("Unsupported down migration");
  }
}
