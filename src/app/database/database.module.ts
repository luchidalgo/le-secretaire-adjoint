import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SelectDatabaseComponent } from "./select-database/select-database.component";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [SelectDatabaseComponent],
  imports: [CommonModule, SharedModule],
})
export class DatabaseModule {}
