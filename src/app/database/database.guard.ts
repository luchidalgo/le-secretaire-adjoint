import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import { DatabaseService } from "./database.service";

@Injectable({
  providedIn: "root",
})
export class DatabaseGuard implements CanActivate, CanActivateChild {
  constructor(
    private router: Router,
    private databaseService: DatabaseService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.databaseService.openedDatabase) {
      return true;
    } else {
      return this.router.parseUrl("/select-database");
    }
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    return this.canActivate(childRoute, state);
  }
}
