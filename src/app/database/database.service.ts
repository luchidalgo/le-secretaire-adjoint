import "reflect-metadata";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Connection, createConnection } from "typeorm";

import { Appointment } from "./entities/appointment";
import { Activity } from "./entities/activity";
import { Publisher } from "./entities/publisher";
import { Migration1643121145136 } from "./migrations/1643121145136-migration";

export const LAST_DATABASE_PATH_LOCAL_STORAGE_KEY =
  "LAST_DATABASE_PATH_LOCAL_STORAGE_KEY";

@Injectable({
  providedIn: "root",
})
export class DatabaseService {
  private _connection?: Connection;
  private _openedDatabase = new BehaviorSubject<string | null>(null);
  openedDatabase$ = this._openedDatabase.asObservable();

  get openedDatabase() {
    return this._openedDatabase.value;
  }

  get connection() {
    if (!this._connection) {
      throw new Error("Database not opened");
    }

    return this._connection;
  }

  async openDatabase(database: string) {
    await this.closeDatabase();

    const connection = await createConnection({
      type: "better-sqlite3",
      database,
      entities: [Activity, Appointment, Publisher],
      migrations: [Migration1643121145136],
      migrationsRun: true,
    });

    if (connection) {
      this._connection = connection;
      this._openedDatabase.next(database);
      localStorage.setItem(LAST_DATABASE_PATH_LOCAL_STORAGE_KEY, database);
    }
  }

  async closeDatabase() {
    this._openedDatabase.next(null);
    localStorage.removeItem(LAST_DATABASE_PATH_LOCAL_STORAGE_KEY);

    if (this._connection?.isConnected) {
      await this._connection.close();
      this._connection = undefined;
    }
  }
}
