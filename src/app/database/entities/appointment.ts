import { Entity, PrimaryGeneratedColumn, Column, Unique, ManyToOne } from "typeorm";
import { Publisher } from "./publisher";

export enum AppointmentType {
  RegularPioneer = "regular-pioneer",
  SpecialPioneer = "special-pioneer",
  AuxiliaryPioneer = "auxiliary-pioneer",
  Elder = "elder",
  MinisterialServant = "ministerial-servant"
}

@Entity("appointment")
export class Appointment {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Publisher, publisher => publisher.appointments)
  publisher: Publisher;
  
  @Column()
  type: AppointmentType;
  
  @Column()
  startDate: Date;

  @Column({ nullable: true })
  endDate?: Date;
}
