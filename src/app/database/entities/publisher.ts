import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Unique,
  OneToMany,
} from "typeorm";
import { Appointment, AppointmentType } from "./appointment";
import { Activity } from "./activity";
import isBefore from "date-fns/isBefore";
import isAfter from "date-fns/isAfter";
import startOfMonth from "date-fns/startOfMonth";
import { endOfMonth } from "date-fns";

@Entity("publisher")
@Unique(["firstName", "lastName"])
export class Publisher {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ nullable: true })
  birthDate?: Date;

  @Column({ nullable: true })
  baptismDate?: Date;

  @Column()
  gender: "male" | "female";

  @Column()
  isAnointed: boolean;

  @Column()
  isInactive: boolean;

  @OneToMany(() => Activity, (activity) => activity.publisher, {
    cascade: true,
  })
  activities: Activity[];

  @OneToMany(() => Appointment, (appointment) => appointment.publisher, {
    cascade: true,
  })
  appointments: Appointment[];

  getAppointmentsTypesByInterval(
    intervalStartDate: Date,
    intervalEndDate: Date
  ): AppointmentType[] {
    return this.appointments
      .filter((appointment) => {
        if (isAfter(appointment.startDate, intervalEndDate)) {
          return false;
        }

        if (
          appointment.endDate &&
          isBefore(appointment.endDate, intervalStartDate)
        ) {
          return false;
        }

        return true;
      })
      .map((appointment) => appointment.type);
  }

  getCurrentAppointmentsTypes(): AppointmentType[] {
    const now = new Date();
    return this.getAppointmentsTypesByInterval(now, now);
  }

  getActivitiesByInterval(intervalStartDate: Date, intervalEndDate: Date) {
    return this.activities.filter((activity) => {
      if (isAfter(startOfMonth(activity.month), intervalEndDate)) {
        return false;
      }

      if (isBefore(endOfMonth(activity.month), intervalStartDate)) {
        return false;
      }

      return true;
    });
  }
}
