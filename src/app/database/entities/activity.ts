import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Unique,
  ManyToOne,
} from "typeorm";
import { Publisher } from "./publisher";

@Entity("activity")
@Unique(["publisher", "month"])
export class Activity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Publisher, (publisher) => publisher.activities)
  publisher: Publisher;

  @Column()
  month: Date;

  @Column()
  placements: number;

  @Column()
  videos: number;

  @Column()
  hours: number;

  @Column()
  visits: number;

  @Column()
  studies: number;

  @Column({ nullable: true })
  remarks?: string;
}
