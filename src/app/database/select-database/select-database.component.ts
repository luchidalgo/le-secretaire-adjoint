import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ipcRenderer } from "electron";
import { Observable } from "rxjs";
import { DatabaseService } from "../database.service";

@Component({
  selector: "app-select-database",
  templateUrl: "./select-database.component.html",
  styleUrls: ["./select-database.component.scss"],
})
export class SelectDatabaseComponent {
  openedDatabase$: Observable<string | null>;

  constructor(
    private databaseService: DatabaseService,
    private router: Router
  ) {
    this.openedDatabase$ = this.databaseService.openedDatabase$;
  }

  closeDatabase() {
    this.databaseService.closeDatabase();
  }

  async openDatabase() {
    const result = await ipcRenderer.invoke("show-open-dialog", {
      title: "Ouvrir une base de données",
      properties: ["openFile"],
      filters: [{ name: "Base de donnée", extensions: ["sqlite"] }],
    });

    if (!result.canceled && result.filePaths.length) {
      await this.databaseService.openDatabase(result.filePaths[0]);
      this.router.navigate([""]);
    }
  }

  async createDatabase() {
    const result = await ipcRenderer.invoke("show-save-dialog", {
      title: "Créer une base de données",
      filters: [{ name: "Base de donnée", extensions: ["sqlite"] }],
    });

    if (!result.canceled && result.filePath) {
      await this.databaseService.openDatabase(result.filePath);
      this.router.navigate([""]);
    }
  }

  async openExplorer(path: string) {
    await ipcRenderer.invoke("open-explorer", path);
  }
}
