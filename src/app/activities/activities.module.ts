import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivitiesRoutingModule } from './activities-routing.module';
import { ListActivitiesComponent } from './list-activities/list-activities.component';
import { SharedModule } from '../shared/shared.module';
import { MonthActivitiesComponent } from './month-activities/month-activities.component';
import { ImportActivitiesComponent } from './import-activities/import-activities.component';


@NgModule({
  declarations: [
    ListActivitiesComponent,
    MonthActivitiesComponent,
    ImportActivitiesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ActivitiesRoutingModule
  ]
})
export class ActivitiesModule { }
