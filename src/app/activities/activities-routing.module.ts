import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ImportActivitiesComponent } from "./import-activities/import-activities.component";
import { ListActivitiesComponent } from "./list-activities/list-activities.component";
import { MonthActivitiesComponent } from "./month-activities/month-activities.component";

const routes: Routes = [
  { path: "", component: ListActivitiesComponent },
  { path: "month-activities/:month", component: MonthActivitiesComponent },
  { path: "import-activities", component: ImportActivitiesComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActivitiesRoutingModule {}
