import { Component, ElementRef, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDatepicker } from "@angular/material/datepicker";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import readXlsxFile from "read-excel-file";
import { DatabaseService } from "src/app/database/database.service";
import { Activity } from "src/app/database/entities/activity";
import { Publisher } from "src/app/database/entities/publisher";
import { ErrorsDialogComponent } from "src/app/shared/errors-dialog/errors-dialog.component";
import { format, isEqual } from 'date-fns'
import { MAT_DATE_FORMATS } from "@angular/material/core";

const MONTH_FORMATS = {
  parse: {
    dateInput: "MM/yyyy",
  },
  display: {
    dateInput: "MM/yyyy",
    monthYearLabel: "MMM yyyy",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM yyyy",
  },
};

@Component({
  selector: "app-import-activities",
  templateUrl: "./import-activities.component.html",
  styleUrls: ["./import-activities.component.scss"],
  providers: [{ provide: MAT_DATE_FORMATS, useValue: MONTH_FORMATS }],
})
export class ImportActivitiesComponent {
  public form: FormGroup;
  @ViewChild("fileInput") fileInput: ElementRef<HTMLInputElement>;
  dataSource = importActivitiesFileFormat;

  constructor(
    private formBuilder: FormBuilder,
    private databaseService: DatabaseService,
    private dialog: MatDialog,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.form = this.formBuilder.group({
      month: [null, Validators.required],
      file: [null, Validators.required],
    });
  }

  get month() {
    return this.form.get("month");
  }

  get file() {
    return this.form.get("file");
  }

  onFileInputCHange() {
    let file = null;

    if (
      this.fileInput.nativeElement.files &&
      this.fileInput.nativeElement.files.length
    ) {
      file = this.fileInput.nativeElement.files[0].name;
    }

    this.form.patchValue({ file });
  }

  async handleSubmit() {
    if (!this.form.valid) {
      return;
    }

    const activityRepository =
      this.databaseService.connection.getRepository(Activity);
    const month = this.form.value.month;
    const file = this.fileInput.nativeElement.files?.item(0);
    if (!file) {
      return;
    }

    const { rows, errors } = await readXlsxFile<ImportActivitiesFileRow>(file, {
      schema: importActivitiesFileSchema,
    });

    if (errors.length > 0) {
      this.showErrors(
        errors.map(
          (error) => `
          Ligne ${error.row}, colonne ${error.column} : 
          donnée ${error.error === "required" ? "manquante" : "incorrecte"}
          `
        )
      );

      this.resetInputFile();
      return;
    }

    const databaseErrors: string[] = [];
    const activities: Activity[] = [];
    for (const row of rows) {
      const publisher = await this.databaseService.connection
        .getRepository(Publisher)
        .createQueryBuilder("publisher")
        .leftJoinAndSelect("publisher.activities", "activity")
        .andWhere("publisher.firstName LIKE :firstName", {
          firstName: row.firstName,
        })
        .andWhere("publisher.lastName LIKE :lastName", {
          lastName: row.lastName,
        })
        .getOne();

      if (!publisher) {
        databaseErrors.push(
          `Le proclamateur ${row.lastName} ${row.firstName} n'existe pas dans la base de donnée`
        );
        continue;
      }

      if (publisher.activities.some((activity) => isEqual(activity.month, month))) {
        databaseErrors.push(
          `L'activité de ${format(month, 'MMM yyyy')} pour ${row.lastName} ${row.firstName} existe déjà dans la base de donnée`
        );
        continue;
      }

      activities.push(
        activityRepository.create({
          publisher,
          month,
          placements: row.placements || 0,
          videos: row.videos || 0,
          hours: row.hours || 0,
          visits: row.visits || 0,
          studies: row.studies || 0,
          remarks: row.remarks
        })
      );
    }

    if (databaseErrors.length > 0) {
      this.showErrors(databaseErrors);
      this.resetInputFile();
      return;
    }

    await activityRepository.insert(activities);

    this.snackBar.open("Activités importés", "Ok", { duration: 5000 });
    this.router.navigate(["/activities"]);
  }

  resetInputFile() {
    this.fileInput.nativeElement.value = "";
    this.fileInput.nativeElement.dispatchEvent(new Event("change"));
  }

  showErrors(errors: string[]) {
    const data = {
      title: "Erreur d'importation",
      message:
        "L'importation n'a pas fonctionnée. Corrigez les erreurs suivantes puis réessayez.",
      errors: errors,
    };

    this.dialog.open(ErrorsDialogComponent, { data });
  }

  chosenMonthHandler(date: Date, datepicker: MatDatepicker<Date>) {
    this.month?.setValue(date);
    datepicker.close();
  }
}

interface ImportActivitiesFileRow {
  firstName: string;
  lastName: string;
  placements?: number;
  videos?: number;
  hours?: number;
  visits?: number;
  studies?: number;
  remarks?: string;
}

const importActivitiesFileSchema = {
  Nom: {
    prop: "lastName",
    type: String,
    required: true,
  },
  Prénom: {
    prop: "firstName",
    type: String,
    required: true,
  },
  Publications: {
    prop: "placements",
    type: Number,
    required: false,
  },
  Vidéos: {
    prop: "videos",
    type: Number,
    required: false,
  },
  Heures: {
    prop: "hours",
    type: Number,
    required: false,
  },
  "Nouvelles visites": {
    prop: "visits",
    type: Number,
    required: false,
  },
  "Cours bibliques": {
    prop: "studies",
    type: Number,
    required: false,
  },
  Observations: {
    prop: "remarks",
    type: String,
    required: false,
  },
};

export const importActivitiesFileFormat: {
  position: number;
  field: string;
  format: string;
  required: boolean;
}[] = [
  { position: 1, field: "Nom", format: "Texte", required: true },
  { position: 2, field: "Prénom", format: "Texte", required: true },
  { position: 3, field: "Publications", format: "Nombre", required: false },
  { position: 4, field: "Vidéos", format: "Nombre", required: false },
  { position: 5, field: "Heures", format: "Nombre", required: false },
  {
    position: 6,
    field: "Nouvelles visites",
    format: "Nombre",
    required: false,
  },
  { position: 7, field: "Cours bibliques", format: "Nombre", required: false },
  { position: 8, field: "Observations", format: "Texte", required: false },
];
