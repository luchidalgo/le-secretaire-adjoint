import { Component, OnInit } from "@angular/core";
import { DatabaseService } from "src/app/database/database.service";
import { Activity } from "src/app/database/entities/activity";
import { Publisher } from "src/app/database/entities/publisher";
import { ipcRenderer } from "electron";
import { PublisherRecordService } from "src/app/shared/publisher-record.service";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: "app-list-activities",
  templateUrl: "./list-activities.component.html",
  styleUrls: ["./list-activities.component.scss"],
})
export class ListActivitiesComponent implements OnInit {
  activitiesByMonth: {
    month: string;
    activitiesCount: number;
  }[];
  publishersCount: number;
  loading = false;

  constructor(
    private databaseService: DatabaseService,
    private publisherRecordService: PublisherRecordService,
    private snackBar: MatSnackBar
  ) {}

  async ngOnInit() {
    this.activitiesByMonth = await this.databaseService.connection
      .getRepository(Activity)
      .createQueryBuilder("activity")
      .select("activity.month", "month")
      .addSelect("COUNT(activity.id)", "activitiesCount")
      .groupBy("activity.month")
      .orderBy("activity.month", "DESC")
      .getRawMany();

    this.publishersCount = await this.databaseService.connection
      .getRepository(Publisher)
      .count();
  }

  async exportPublishersRecords() {
    const result = await ipcRenderer.invoke("show-open-dialog", {
      title: "Exporter les S-21",
      properties: ["openDirectory"],
    });

    if (result.canceled || !result.filePaths[0]) {
      return;
    }

    const folderPath = result.filePaths[0];
    this.loading = true;
    await this.publisherRecordService.exportPublishersRecords(folderPath);
    this.loading = false;
    this.snackBar
      .open("Les S-21 ont été générées", "Ouvrir", { duration: 5000 })
      .onAction()
      .subscribe(() => {
        ipcRenderer.invoke("open-explorer", folderPath);
      });
  }
}
