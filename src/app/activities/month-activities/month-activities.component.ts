import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { DatabaseService } from "src/app/database/database.service";
import { Activity } from "src/app/database/entities/activity";
import { AppointmentType } from "src/app/database/entities/appointment";
import {
  endOfMonth,
  format,
  startOfMonth,
} from "date-fns";
import { MatSnackBar } from "@angular/material/snack-bar";
import writeXlsxFile from "write-excel-file";

@Component({
  selector: "app-month-activities",
  templateUrl: "./month-activities.component.html",
  styleUrls: ["./month-activities.component.scss"],
})
export class MonthActivitiesComponent implements OnInit {
  activities: Activity[];
  recaps: {
    type: string;
    count: number;
    placements: number;
    videos: number;
    hours: number;
    visits: number;
    studies: number;
  }[];
  month: Date;

  constructor(
    private databaseService: DatabaseService,
    private route: ActivatedRoute
  ) {}

  async ngOnInit() {
    this.month = new Date(this.route.snapshot.paramMap.get("month")!);

    this.activities = await this.databaseService.connection
      .getRepository(Activity)
      .createQueryBuilder("activity")
      .innerJoinAndSelect("activity.publisher", "publisher")
      .leftJoinAndSelect("publisher.appointments", "appointment")
      .where("DATE(activity.month) = DATE(:month)", {
        month: this.month.toISOString(),
      })
      .orderBy("lastName", "ASC")
      .addOrderBy("firstName", "ASC")
      .getMany();

    this.recaps = [
      this.getRegularPioneersRecap(),
      this.getAuxiliaryPioneersRecap(),
      this.getPublishersRecap(),
    ];
  }

  getPublishersRecap() {
    const activities = this.activities.filter((activity) => {
      if (activity.publisher.isInactive) {
        return false;
      }

      const appointmentTypes =
        activity.publisher.getAppointmentsTypesByInterval(
          startOfMonth(this.month),
          endOfMonth(this.month)
        );

      if (appointmentTypes.includes(AppointmentType.RegularPioneer)) {
        return false;
      }
      if (appointmentTypes.includes(AppointmentType.AuxiliaryPioneer)) {
        return false;
      }
      if (appointmentTypes.includes(AppointmentType.SpecialPioneer)) {
        return false;
      }

      return true;
    });

    return { type: "publisher", ...this.getActivitiesRecap(activities) };
  }

  getRegularPioneersRecap() {
    const activities = this.activities.filter((activity) => {
      if (activity.publisher.isInactive) {
        return false;
      }

      return activity.publisher
        .getAppointmentsTypesByInterval(
          startOfMonth(this.month),
          endOfMonth(this.month)
        )
        .includes(AppointmentType.RegularPioneer);
    });

    return { type: "regular-pioneer", ...this.getActivitiesRecap(activities) };
  }

  getAuxiliaryPioneersRecap() {
    const activities = this.activities.filter((activity) => {
      if (activity.publisher.isInactive) {
        return false;
      }

      return activity.publisher
        .getAppointmentsTypesByInterval(
          startOfMonth(this.month),
          endOfMonth(this.month)
        )
        .includes(AppointmentType.AuxiliaryPioneer);
    });

    return {
      type: "auxiliary-pioneer",
      ...this.getActivitiesRecap(activities),
    };
  }

  getActivitiesRecap(activities: Activity[]): {
    count: number;
    placements: number;
    videos: number;
    hours: number;
    visits: number;
    studies: number;
  } {
    return {
      count: activities.length,
      ...activities
        .map((activity) => {
          return {
            placements: activity.placements,
            videos: activity.videos,
            hours: activity.hours,
            visits: activity.visits,
            studies: activity.studies,
          };
        })
        .reduce(
          (accumulator, current) => {
            return {
              placements: accumulator.placements + current.placements,
              videos: accumulator.videos + current.videos,
              hours: accumulator.hours + current.hours,
              visits: accumulator.visits + current.visits,
              studies: accumulator.studies + current.studies,
            };
          },
          {
            placements: 0,
            videos: 0,
            hours: 0,
            visits: 0,
            studies: 0,
          }
        ),
    };
  }
  async exportData() {
    await writeXlsxFile(this.activities, {
      schema: exportSchema,
      headerStyle: {
        backgroundColor: '#eeeeee',
        fontWeight: 'bold',
        align: 'center'
      },
      fileName: `Activités ${format(this.month, "MMMM yyyy")}.xlsx`,
    });
  }
}

const exportSchema = [
  {
    column: "Nom",
    type: String,
    value: (activity: Activity) => activity.publisher.lastName,
  },
  {
    column: "Prénom",
    type: String,
    value: (activity: Activity) => activity.publisher.firstName,
  },
  {
    column: "Publications",
    type: Number,
    value: (activity: Activity) => activity.placements,
  },
  {
    column: "Vidéos",
    type: Number,
    value: (activity: Activity) => activity.videos,
  },
  {
    column: "Heures",
    type: Number,
    value: (activity: Activity) => activity.hours,
  },
  {
    column: "Visites",
    type: Number,
    value: (activity: Activity) => activity.visits,
  },
  {
    column: "Etudes",
    type: Number,
    value: (activity: Activity) => activity.studies,
  },
  {
    column: "Observations",
    type: String,
    value: (activity: Activity) => activity.remarks || "",
  },
];
