export const s21FormFields = {
  publisher: {
    name: '900_1_Text',
    birthDate: '900_2_Text',
    baptismDate: '900_3_Text',
    male: '900_4_CheckBox',
    female: '900_5_CheckBox',
    anointed: '900_7_CheckBox',
    notAnointed: '900_6_CheckBox',
    serviceYear: '900_11_Year_C',
    elder: '900_8_CheckBox',
    ministerialServant: '900_9_CheckBox',
    regularPioneer: '900_10_CheckBox',
  },
  month: (month: number) => {
    month = month + 5;
    if(month > 12) {
      month = month - 12;
    }
    return {
      placements: `901_${month}_S21_Value`,
      videos: `902_${month}_S21_Value`,
      hours: `903_${month}_S21_Value`,
      visits: `904_${month}_S21_Value`,
      studies: `905_${month}_S21_Value`,
      remarks: `906_${month}_Text_Scroll`,
    };
  },
  total: {
    placements: `901_13_S21_Value`,
    videos: `902_13_S21_Value`,
    hours: `903_13_S21_Value`,
    visits: `904_13_S21_Value`,
    studies: `905_13_S21_Value`,
    remarks: `906_13_Text_Scroll`,
  },
  average: {
    placements: `901_14_S21_Value`,
    videos: `902_14_S21_Value`,
    hours: `903_14_S21_Value`,
    visits: `904_14_S21_Value`,
    studies: `905_14_S21_Value`,
    remarks: `906_14_Text_Scroll`,
  }
};
