import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SpinnerOverlayComponent } from "./spinner-overlay/spinner-overlay.component";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatCardModule } from "@angular/material/card";
import { MatTableModule } from "@angular/material/table";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatNativeDateModule } from "@angular/material/core";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatSelectModule } from "@angular/material/select";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatRadioModule } from "@angular/material/radio";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatListModule } from "@angular/material/list";
import { MatDividerModule } from "@angular/material/divider";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatToolbarModule } from "@angular/material/toolbar";
import { ReactiveFormsModule } from "@angular/forms";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatIconModule } from "@angular/material/icon";
import { MatMenuModule } from "@angular/material/menu";
import { MatDateFnsModule } from "@angular/material-date-fns-adapter";
import { ConfirmDialogComponent } from "./confirm-dialog/confirm-dialog.component";
import { ErrorsDialogComponent } from "./errors-dialog/errors-dialog.component";

const materialComponents = [
  FlexLayoutModule,
  MatProgressSpinnerModule,
  MatCardModule,
  MatTableModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCheckboxModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatRadioModule,
  MatDialogModule,
  MatSnackBarModule,
  MatTooltipModule,
  MatToolbarModule,
  MatSidenavModule,
  MatDividerModule,
  MatListModule,
  MatAutocompleteModule,
  MatIconModule,
  MatMenuModule,
  MatDateFnsModule,
];

@NgModule({
  declarations: [
    SpinnerOverlayComponent,
    ConfirmDialogComponent,
    ErrorsDialogComponent,
  ],
  imports: [...materialComponents, CommonModule, ReactiveFormsModule],
  exports: [
    ...materialComponents,
    ReactiveFormsModule,
    SpinnerOverlayComponent,
  ],
  providers: [],
})
export class SharedModule {}
