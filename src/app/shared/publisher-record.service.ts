import { Injectable } from "@angular/core";
import { DatabaseService } from "../database/database.service";
import { Publisher } from "../database/entities/publisher";
import { PDFDocument, PDFForm } from "pdf-lib";
import { AppointmentType } from "../database/entities/appointment";
import { promises as fs } from "fs";
import { s21FormFields } from "./s21-form-fields";
import { Activity } from "../database/entities/activity";

@Injectable({
  providedIn: "root",
})
export class PublisherRecordService {
  constructor(private databaseService: DatabaseService) {}

  async exportPublishersRecords(basePath: string) {
    const baseFile = await fetch(`./assets/pdf/S-21_F.pdf`).then((res) =>
      res.arrayBuffer()
    );
    const serviceYear = this.getServiceYear();
    const publishers = await this.databaseService.connection
      .getRepository(Publisher)
      .find({ relations: ["appointments", "activities"] });

    for (const publisher of publishers) {
      const folderPath = this.getPublisherRecordFolderPath(publisher);
      await fs.mkdir(`${basePath}${folderPath}`, { recursive: true });

      await fs.writeFile(
        `${basePath}${folderPath}\\S-21 - ${publisher.lastName} ${publisher.firstName} - ${serviceYear}.pdf`,
        await this.getPublisherRecordFile(publisher, serviceYear, baseFile)
      );
      await fs.writeFile(
        `${basePath}${folderPath}\\S-21 - ${publisher.lastName} ${
          publisher.firstName
        } - ${serviceYear - 1}.pdf`,
        await this.getPublisherRecordFile(publisher, serviceYear - 1, baseFile)
      );
    }
  }


  private async getPublisherRecordFile(
    publisher: Publisher,
    serviceYear: number,
    baseFile: ArrayBuffer
  ) {
    const pdfDoc = await PDFDocument.load(baseFile);
    const form = pdfDoc.getForm();
    const appointmentTypes = publisher.getAppointmentsTypesByInterval(
      new Date(`${serviceYear}-09-01`),
      new Date(`${serviceYear + 1}-08-31`)
    );

    this.fillPublisherRecordHeaderForm(
      form,
      publisher,
      serviceYear,
      appointmentTypes
    );

    const activities = publisher.getActivitiesByInterval(
      new Date(`${serviceYear}-09-01`),
      new Date(`${serviceYear + 1}-08-31`)
    );

    this.fillPublisherRecordActivitiesForm(form, activities);
    this.fillPublisherRecordRecapForm(form, activities);

    return pdfDoc.save();
  }

  private fillPublisherRecordActivitiesForm(
    form: PDFForm,
    activities: Activity[]
  ) {
    for (const activity of activities) {
      const s21FormMonthFields = s21FormFields.month(activity.month.getMonth());

      form
        .getTextField(s21FormMonthFields.placements)
        .setText(activity.placements.toString());

      form
        .getTextField(s21FormMonthFields.videos)
        .setText(activity.videos.toString());

      form
        .getTextField(s21FormMonthFields.hours)
        .setText(activity.hours.toString());

      form
        .getTextField(s21FormMonthFields.visits)
        .setText(activity.visits.toString());

      form
        .getTextField(s21FormMonthFields.studies)
        .setText(activity.studies.toString());

      form
        .getTextField(s21FormMonthFields.remarks)
        .setText(activity.remarks || "");
    }
  }

  private fillPublisherRecordRecapForm(form: PDFForm, activities: Activity[]) {
    if (activities.length == 0) {
      return;
    }

    const reducer = (accumulator: number, current: number) =>
      accumulator + current;

    const s21FormTotalFields = s21FormFields.total;
    const s21FormAverageFields = s21FormFields.average;
    const count = activities.length;
    const totals = {
      placements: activities
        .map((activity) => activity.placements)
        .reduce(reducer, 0),
      videos: activities.map((activity) => activity.videos).reduce(reducer, 0),
      hours: activities.map((activity) => activity.hours).reduce(reducer, 0),
      visits: activities.map((activity) => activity.visits).reduce(reducer, 0),
      studies: activities
        .map((activity) => activity.studies)
        .reduce(reducer, 0),
    };

    form
      .getTextField(s21FormTotalFields.placements)
      .setText(totals.placements.toString());

    form
      .getTextField(s21FormTotalFields.videos)
      .setText(totals.videos.toString());

    form
      .getTextField(s21FormTotalFields.hours)
      .setText(totals.hours.toString());

    form
      .getTextField(s21FormTotalFields.visits)
      .setText(totals.visits.toString());

    form
      .getTextField(s21FormTotalFields.studies)
      .setText(totals.studies.toString());

    form
      .getTextField(s21FormAverageFields.placements)
      .setText(this.round(totals.placements / count).toString());

    form
      .getTextField(s21FormAverageFields.videos)
      .setText(this.round(totals.videos / count).toString());

    form
      .getTextField(s21FormAverageFields.hours)
      .setText(this.round(totals.hours / count).toString());

    form
      .getTextField(s21FormAverageFields.visits)
      .setText(this.round(totals.visits / count).toString());

    form
      .getTextField(s21FormAverageFields.studies)
      .setText(this.round(totals.studies / count).toString());
  }

  private fillPublisherRecordHeaderForm(
    form: PDFForm,
    publisher: Publisher,
    serviceYear: number,
    appointmentTypes: AppointmentType[]
  ) {
    form
      .getTextField(s21FormFields.publisher.name)
      .setText(`${publisher.lastName} ${publisher.firstName}`);
    form
      .getTextField(s21FormFields.publisher.serviceYear)
      .setText(`${serviceYear} / ${serviceYear + 1}`);

    if (publisher.birthDate) {
      form
        .getTextField(s21FormFields.publisher.birthDate)
        .setText(publisher.birthDate.toLocaleDateString());
    }

    if (publisher.baptismDate) {
      form
        .getTextField(s21FormFields.publisher.baptismDate)
        .setText(publisher.baptismDate.toLocaleDateString());
    }

    if (publisher.gender === "male") {
      form.getCheckBox(s21FormFields.publisher.male).check();
    } else {
      form.getCheckBox(s21FormFields.publisher.female).check();
    }

    if (publisher.isAnointed) {
      form.getCheckBox(s21FormFields.publisher.anointed).check();
    } else {
      form.getCheckBox(s21FormFields.publisher.notAnointed).check();
    }

    if (appointmentTypes.includes(AppointmentType.Elder)) {
      form.getCheckBox(s21FormFields.publisher.elder).check();
    }

    if (appointmentTypes.includes(AppointmentType.MinisterialServant)) {
      form.getCheckBox(s21FormFields.publisher.ministerialServant).check();
    }

    if (appointmentTypes.includes(AppointmentType.RegularPioneer)) {
      form.getCheckBox(s21FormFields.publisher.regularPioneer).check();
    }
  }

  private getPublisherRecordFolderPath(publisher: Publisher) {
    let filePath = "";
    if (publisher.isInactive) {
      filePath += "\\Inactifs";
    } else {
      filePath += "\\Actifs";

      const appointmentTypes = publisher.getCurrentAppointmentsTypes();
      if (
        appointmentTypes.includes(AppointmentType.RegularPioneer) ||
        appointmentTypes.includes(AppointmentType.SpecialPioneer)
      ) {
        filePath += "\\Pionniers permanents et spéciaux";
      } else {
        filePath += "\\Proclamateurs";
      }
    }

    filePath += `\\${publisher.lastName} ${publisher.firstName}`;

    return filePath;
  }

  private getServiceYear() {
    const now = new Date();
    let serviceYear = now.getFullYear();
    if (now.getMonth() < 8) {
      serviceYear--;
    }
    return serviceYear;
  }

  private round(number: number): number {
    return Math.round((number + Number.EPSILON) * 100) / 100;
  }
}
