const { app, BrowserWindow, ipcMain, dialog, shell } = require("electron");
const path = require("path");
process.env.TZ = "UTC";

if (require("electron-squirrel-startup")) {
  app.quit();
}

const createWindow = () => {
  const mainWindow = new BrowserWindow({
    height: 800,
    width: 1000,
    titleBarStyle: "hidden",
    titleBarOverlay: {
      color: "#f3f3f3",
    },
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  const env = process.env.NODE_ENV;
  if (env === "development") {
    mainWindow.loadURL(`http://localhost:4200`);
    mainWindow.webContents.openDevTools();
  } else {
    const indexPath = path.join(__dirname, "../renderer/index.html");
    mainWindow.loadURL(`file://${indexPath}`);
  }
};

app.whenReady().then(() => {
  createWindow();

  app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

ipcMain.handle("show-open-dialog", (event, options) => {
  return dialog.showOpenDialog(options);
});

ipcMain.handle("show-save-dialog", (event, options) => {
  return dialog.showSaveDialog(options);
});

ipcMain.handle("open-explorer", (event, path) => {
  shell.showItemInFolder(path);
});
