module.exports = {
  packagerConfig: {
    icon: "./src/favicon.ico",
  },
  makers: [
    {
      name: "@electron-forge/maker-squirrel",
      config: {
        title: "Le secrétaire adjoint",
        name: "le-secretaire-adjoint",
        setupIcon: "./src/favicon.ico",
      },
    },
  ],
  publishers: [
    {
      name: "@electron-forge/publisher-bitbucket",
      config: {
        replaceExistingFiles: true,
        repository: {
          owner: "luchidalgo",
          name: "le-secretaire-adjoint",
        },
      },
    },
  ],
  plugins: [
    [
      "@electron-forge/plugin-webpack",
      {
        mainConfig: "./electron/forge.webpack.js",
        renderer: {
          entryPoints: [
            {
              js: "./electron/renderer.js",
              name: "main_window",
            },
          ],
        },
      },
    ],
    [
      "@timfish/forge-externals-plugin",
      {
        externals: ["typeorm", "better-sqlite3"],
      },
    ],
  ],
};
