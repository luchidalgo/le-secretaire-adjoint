const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './electron/main.js',
  resolve: {
    extensions: ['.js', '.ts', '.jsx', '.tsx', '.css', '.json']
  },
	plugins: [
		new CopyWebpackPlugin({
			patterns: [
				{
					from: 'dist',
					to: '../renderer',
					noErrorOnMissing: true,
				},
			],
		}),
	],
  optimization: {
    minimize: false,
  }
};